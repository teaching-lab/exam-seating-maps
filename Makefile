# Makefile to create PDF documents from LaTeX-Files 
# Needed software packages: pdflatex, rubber 

TEXS=$(wildcard rooms/*.tex)
PDFS=$(TEXS:.tex=.pdf)

all: $(PDFS) clean 

%.pdf: %.tex exam_seating_map.cls
	pdflatex --output-directory=rooms $< 

clean: 
	rm -f rooms/*.aux rooms/*.log rooms/*.out

cleanpdfs: 
	rm -f rooms/*.pdf 

.PHONY: all clean 

