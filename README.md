Exam seating maps
=================

Simplified floorplans of FI MU lecture rooms for easy exam seating. The individual files contain basic seating patterns for different rooms and number of students, as well as multi-variant exams (A/B/C/D). A laminated version of the plan should also be available in the room.

## Room overview

* The capacities below differentiate wheelchair spots (the extra number in parenthesis).
* Count on some tables/chairs being broken. When identified, please report them to bud@fi.muni.cz.

| Room | Capacity | Note |
|------|----------|------|
| D1   | 248      |      |
| D3   | 179      |      |
| D2   | 122      |      |
| A107 |          |      |
| A217 | 76       |      | 
| A318 | 76       |      | 
| B204 | 56       |      |
| A218 |          |      |
| B130 | 50       | pc   |
| A219 |          | pc   |

## Building

The provided Makefile has four targets

1. make - create PDF of all .tex files in current directory
2. make <file>.pdf - mode for single file conversion, convert <file>.tex to <file>.pdf
3. make clean - Clean directory from unnecessary files *.tox, *.aux, *.log, *.out
4. make cleanall - same as cleanall, but *.pdf files are also removed

## Contributing

* Each lecture room is in a separate file (all variants).
* When adding variants, keep a reasonable document width to ensure printing on a single A4 page without page wrapping.

## Authors

This work is a colaborative effort in the spirit of Open culture.
The alphabetical thanks belong to Jiří Barnat, Jakub Bartolomej Košuth, Vojtěch Řehák, Libor Škarvada and Martin Ukrop.
