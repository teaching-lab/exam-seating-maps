\ProvidesClass{exam_seating_map}[2019/06/23 v1.2 Exam seating map template]

% Based on 'article'
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions \relax
\LoadClass{article}

% Load necessary packages
\RequirePackage[a4paper, top=2.5cm, bottom=3cm, left=1.5cm, right=1.5cm]{geometry}
\RequirePackage{fancyhdr}
\RequirePackage{xcolor}
\RequirePackage{graphicx}
\RequirePackage{amsmath,amsthm,amsfonts,amssymb,amscd}
\RequirePackage[english]{babel}
\RequirePackage{url}

% Define header strings
\def\@capacity{}
\newcommand{\setCapacity}[1]{\def\@capacity{#1}} % <-- Set capacity
\def\@dimensions{}
\newcommand{\setDimensions}[1]{\def\@dimensions{#1}} % <-- Set dimensions
\def\@room{}
\newcommand{\setRoom}[1]{\def\@room{#1}} % <-- Set room

\pagestyle{fancyplain}
\headheight 35pt
\lhead{\textbf{Capacity}}
\lhead{\textbf{Capacity} \\ \@capacity}
\chead{\textbf{\Huge \textbf{Room:} \@room}}
\rhead{\textbf{Dimensions} \\ \@dimensions}
\lfoot{}
\cfoot{\small Version from \today \\ Contributions and issues welcome at \url{https://gitlab.fi.muni.cz/teaching-lab/exam-seating-maps}.}
\rfoot{}
\headsep 1.5em

% Define phantom characters shorthands
\newcommand{\p}[1]{\phantom{#1}}
